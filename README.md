# Health manager
## Приложение для хранения медицинских данных пользователя
## Сбирается с помощью [docker-compose.yml](https://gitlab.com/health-manager1/utils/-/blob/main/docker-compose.yml)
## Архитектура
### * Medical history service
### Хранит информацию о посещениях врачей и выписанных лекарствах
Метод | Апи | Описание | Аутентификация 
----------------|------------------|------------------|------------------
 Get | /medhistory/medicines/{medicine_id} | Получение лекарства по id | +
 Post | /medhistory/medicines | Создание лекарства | +
 Put | /medhistory/medicines | Обновление лекарства | +
 Delete | /medhistory/medicines/{medicine_id} | Удаление лекарства по id | +
 Get | /medhistory/visits/{visit_id} | Получение посещения по id | +
 Post | /medhistory/visits | Создание посещения | +
 Put | /medhistory/visits | Обновление посещения | +
 Delete | /medhistory/visits/{visit_id} | Удаление посещения по id | +

### * User management service
### Сервис авторизации пользователя
Метод | Апи | Описание | Аутентификация 
----------------|------------------|------------------|------------------
 Post | /auth/register | Регистрация пользователя | -
 Post | /auth/token | Выдача токена | -
 Get | /auth/validate | Проверка токена | +
### * Gateway
### Эндпоинт http://localhost:8080
### * Discovery service
### Эндпоинт http://localhost:8761

